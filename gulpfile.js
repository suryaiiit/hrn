var gulp = require('gulp');
var watch = require('gulp-watch');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var rename = require("gulp-rename");


gulp.task('reset-css', function() {
  return gulp.src('./sass/reset.sass')
    .pipe(sass().on('error', sass.logError))
    // .pipe(cleanCSS())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./css'));
});

var indexSassFiles = [
  './sass/values.sass',
  './sass/mixins.sass',
  './sass/index.sass',
];

gulp.task('index-css', function() {
  return gulp.src(indexSassFiles)
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('index.css'))
    // .pipe(cleanCSS())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./css'));
});

gulp.task('css', ['reset-css', 'index-css']);

gulp.task('watch', function() {
  gulp.watch('./sass/**/*.sass', ['css']);
});

gulp.task('default', ['reset-css', 'index-css', 'watch']);
